# Choose between "pdf" and "dvi".
OUTPUT_FORMAT = pdf

# This should work well for most LaTeX distributions.
TEX_COMPILER = pdflatex -interaction nonstopmode -file-line-error -halt-on-error -output-format $(OUTPUT_FORMAT)

# We use "biber" instead of "bibtex", because it can handle the UTF-8 encoding.
# The "biber" package is available in official Debian/Ubuntu repositories.
BIB_COMPILER = biber

# We track changes in all files.
INPUTS = $(shell find . -type f)

# Pretty obvious choice, isn't it?
OUTPUT = main.$(OUTPUT_FORMAT)

.PHONY: clean

# Compile the document.
$(OUTPUT): $(INPUTS)
	$(TEX_COMPILER) main
	$(BIB_COMPILER) main
	$(TEX_COMPILER) main
	$(TEX_COMPILER) main

# Remove all auxiliary files.
clean:
	rm -f $(shell ls -1 main.* | grep -v main.tex)
	rm -f $(shell find . -type f -name "*.aux")
