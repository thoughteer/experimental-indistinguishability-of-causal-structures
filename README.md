My essay on the philosophy of science for the upcoming exam.
It is, basically, a translation of an interesting [paper][source], published in
the *Philosophy of Science* journal by Frederick Eberhardt, into Russian.

[source]: http://authors.library.caltech.edu/44031/1/673865.pdf